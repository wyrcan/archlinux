ARG TAG=latest
FROM archlinux:${TAG}

RUN pacman -Sy --noconfirm \
 && pacman -S  --noconfirm linux systemd \
 && pacman -Sc --noconfirm

RUN cd /boot; ln -s vmlinuz* wyrcan.kernel
RUN systemd-firstboot --timezone=UTC
RUN ln -s /lib/systemd/systemd /init
